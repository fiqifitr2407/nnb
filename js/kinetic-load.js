function loadImages(sources, callback, element, width, height) {
        var images = {};
        var loadedImages = 0;
        var numImages = 0;
        // get num of sources
        for(var src in sources) {
          numImages++;
        }
        for(var src in sources) {
          images[src] = new Image();
          images[src].onload = function() {
            if(++loadedImages >= numImages) {
              callback(images, element, width, height);
            }
          };
          images[src].src = sources[src];
        }
}

function loadlogoImages(sources, callback, element, width, height, color, brcolor, border, hlink) {
        var images = {};
        var loadedImages = 0;
        var numImages = 0;
        // get num of sources
        for(var src in sources) {
          numImages++;
        }
        for(var src in sources) {
          images[src] = new Image();
          images[src].onload = function() {
            if(++loadedImages >= numImages) {
              callback(images, element, width, height, color, brcolor, border, hlink);			 
            }
          };
          images[src].src = sources[src];
        }
}

function draw(images, element, width, height) {		
		var xwidth = 0;
		var yheight = 0;
		var radius = 0;
		
		xwidth = width/2;
		yheight = height/2;
		radius = xwidth - 5;
				
        var stage = new Kinetic.Stage({
          container: element,
          width: width,
          height: height
        });
		
        var layer = new Kinetic.Layer();		
       
        var patternPentagon = new Kinetic.RegularPolygon({
          x: xwidth,
          y: yheight,
          sides: 9,
          radius: radius,
          fillPatternImage: images.darthVader,
          fillPatternOffset: {x:xwidth - 5, y:yheight - 5},		  
          stroke: '',
          strokeWidth: 0,
          draggable: false,		 
        });
				
        patternPentagon.on('mouseover touchstart', function() {		 
		  if( images.yoda.src ) {
          	this.fillPatternImage(images.yoda);
          	this.fillPatternOffset({x:xwidth - 5, y:yheight - 5});          	
		  }
		  layer.draw();
        });		

        patternPentagon.on('mouseout touchend', function() {		
		  this.fillPatternImage(images.darthVader);
		  this.fillPatternOffset({x:xwidth - 5, y:yheight - 5});		
		  layer.draw();
        });
	
        layer.add(patternPentagon);
		layer.draw();
		
		stage.add(layer);
		layer.draw();
}

function drawcolor(element, width, height, color, hvcolor, brcolor, border, brhvcolor, icon, iconsize, iconcolor, parent, animateitem, timeout, hlink, target) {	
		var xwidth = 0;
		var yheight = 0;
		var radius = 0;
		
		xwidth = width/2;
		yheight = height/2;
		radius = xwidth - 5;
		
        var stage = new Kinetic.Stage({
          container: element,
          width: width,
          height: height
        });
		
        var layer = new Kinetic.Layer();
				
		var colorPentagon = new Kinetic.RegularPolygon({
          x: xwidth,
          y: yheight,
          sides: 9,
          radius: radius,
          fill: color,
          stroke: brcolor,
          strokeWidth: border,
          draggable: false		  
        });
		
		var text = new Kinetic.Text({
			x: xwidth,
			y: yheight,
			fontFamily: 'FontAwesome',
			fontSize: iconsize,
			text: String.fromCharCode(icon),
			fill: iconcolor,
			align: 'center',			
		});
		
		text.offsetX(text.width()/2);
		text.offsetY(text.height()/2);
		
		colorPentagon.on('click touchstart', function() {
		  if( hlink != '' && hlink != "undefined" ) {			
		  	window.open(hlink, target);
		  }
        });
		
		text.on('click touchstart', function() {
		  if( hlink != '' && hlink != "undefined" ) {
		  	window.open(hlink, target);
		  }
        });
	
		colorPentagon.on('mouseover touchstart', function() {
		  if( hvcolor != '' ) {
		  	this.fill(hvcolor);
		  }
		  if( brhvcolor != '' ) {
		  	this.stroke(brhvcolor);
		  }
          layer.draw();		 	  
        });
		
        colorPentagon.on('mouseout touchend', function() {
          if( color != '' ) {
		  	this.fill(color);
		  }		  
		  if( brcolor != '' ) {
		  	this.stroke(brcolor);
		  }
          layer.draw();		
        });
		
		text.on('mouseover touchstart', function() {
		  if( hvcolor != '' ) {
		  	colorPentagon.fill(hvcolor);
		  }		  
		  if( brhvcolor != '' ) {
		  	colorPentagon.stroke(brhvcolor);
		  }	
          layer.draw();		  
        });
		
		text.on('mouseout touchend', function() {
		  if( color != '' ) {
		  	colorPentagon.fill(color);
		  }		  
		  if( brcolor != '' ) {
		  	colorPentagon.stroke(brcolor);
		  }
          layer.draw();		 
        });	
		
		layer.add(colorPentagon);
		layer.add(text);
		layer.draw();
		
		if( timeout != '' && timeout != "undefined" ) {
			setTimeout(function(){			
				stage.add(layer);
				layer.draw();
			}, timeout);			
		} else {
			setTimeout(function(){			
				stage.add(layer);
				layer.draw();
			}, 8000);	
		}
				
		var tween = new Kinetic.Tween({
			node: colorPentagon, 
			duration: 2,			
			rotation: 360,
			easing: Kinetic.Easings.StrongEaseInOut,
			opacity: 1,
      	});
		
		var tween1 = new Kinetic.Tween({
			node: text, 
			duration: 2,			
			rotation: 360,
			easing: Kinetic.Easings.StrongEaseInOut,
			opacity: 1,
      	});
		
		$( "."+ parent + " ."+ element ).each(function() {
			var have_elem = $(this).find(element);
			if( have_elem ) {
				$( "."+ parent + " ."+ element ).hover(function() {
					if( animateitem == "both" ) {
						tween.play();
						tween1.play();
					} else if( animateitem == "text" ) {
						tween1.play();
					} else if( animateitem == "shape" ) {
						tween.play();
					} else if( animateitem == "socialicon" ) {
						colorPentagon.sides(6);
						colorPentagon.opacity(0.7);
						layer.draw();
					}
				});
				$( "."+ parent + " ."+ element ).mouseleave(function() {								
					if( animateitem == "both" ) {
						tween1.reverse();
						tween.reverse();
					} else if( animateitem == "text" ) {
						tween1.reverse();
					} else if( animateitem == "shape" ) {
						tween.reverse();
					} else if( animateitem == "socialicon" ) {
						colorPentagon.sides(9);
						colorPentagon.opacity(1);
						layer.draw();
					}
				});
			}
		});		
}

function drawlogo(images, element, width, height, color, brcolor, border, hlink) {	
		var xwidth = 0;
		var yheight = 0;
		var radius = 0;
		
		xwidth = width/2;
		yheight = height/2;
		radius = xwidth - 5;
				
        var stage = new Kinetic.Stage({
          container: element,
          width: width,
          height: height
        });
		
        var layer = new Kinetic.Layer();
		
		var colorPentagon = new Kinetic.RegularPolygon({
          x: xwidth,
          y: yheight,
          sides: 9,
          radius: radius,
          fill: color,
          stroke: brcolor,
          strokeWidth: border,
          draggable: false		  
        });
       
        var patternImg = new Kinetic.Image({
		  x: -24,
		  y: 49,
		  width: 165,
		  height: 165,		
          image: images.darthVader,                   
          draggable: false,
		  rotation: -27
        });
		
		colorPentagon.on('click touchstart', function() {
		  if( hlink != '' ) {
		  	window.open(hlink, "_self");
		  }
        });
		
		patternImg.on('click touchstart', function() {
		  if( hlink != '' ) {
		  	window.open(hlink, "_self");
		  }          
        });
		
        layer.add(colorPentagon);
		layer.add(patternImg);
		layer.draw();

		stage.add(layer);
		layer.draw();
}