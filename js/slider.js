(function($){
	$(document).ready(function() {
		var image_array = new Array();
		image_array = [
			{image: 'images/team/t1.jpg'}, // image for the first layer, goes with the text from id="sw0"				
			{image: 'images/team/t2.jpg'}, // image for the second layer, goes with the text from id="sw1"				
			{image: 'images/team/t3.jpg'}, // image for the third layer, goes with the text from id="sw2"				
			{image: 'images/team/t4.jpg'},				
			{image: 'images/team/t5.jpg'},
			{image: 'images/team/t6.jpg'},
			{image: 'images/team/t3.jpg'}
		];
		var border_calc;
		
		$('#slider1').content_slider({		// bind plugin to div id="slider1"
			map : image_array,				// pointer to the image map
			max_shown_items: 7,				// number of visible circles
			small_pic_width: 170,
			small_pic_height: 168,
			main_circle_position: 0,
			left_offset:0,
			hv_switch: 0,					// 0 = horizontal slider, 1 = vertical
			active_item: 0,					// layer that will be shown at start, 0=first, 1=second...
			wrapper_text_max_height: 750,	// height of widget, displayed in pixels
			middle_click: 1, // when main circle is clicked: 1 = slider will go to the previous layer/circle, 2 = to the next
			under_600_max_height: 1350,		// if resolution is below 600 px, set max height of content
			child_div_width: 210,
			border_radius: 10,				// -1 = circle, 0 and other = radius
			automatic_height_resize: 1,
			border_on_off: 0,
			enable_mousewheel: 0,
			allow_shadow: 0
		});
		
		$("a[rel^='prettyPhoto']").prettyPhoto();
	});
})(jQuery);