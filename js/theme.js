/* ==============================================
Page Loader
=============================================== */
$(window).load(function() {
	'use strict';
	$(".loader-item").delay(700).fadeOut();
	$("#pageloader").delay(1200).fadeOut("slow");
	canvasElementsInit();
});

/* ==============================================
Mobile Menu Button
=============================================== */
$('.mini-nav-button').click(function() {
    $(".nav-menu").slideToggle("9000");
});

$('.nav a').click(function () {
	if ($(window).width() < 970) {
    	$(".nav-menu").slideToggle("2000")}
});

/* ==============================================
Client
=============================================== */	
$(window).load(function() {
	$("#flexiselDemo").flexisel({
		visibleItems: 3,
		animationSpeed: 1000,
		autoPlay: true,
		autoPlaySpeed: 5000,
		pauseOnHover: true,
		enableResponsiveBreakpoints: true,
		responsiveBreakpoints: { 
			portrait: { 
				changePoint:480,
				visibleItems: 1
			}, 
			landscape: { 
				changePoint:640,
				visibleItems: 2
			},
			tablet: { 
				changePoint:768,
				visibleItems: 3
			}
		}
	});
});

/* ==============================================
Flex Slider Home Page
=============================================== */		
$(window).load(function(){
	  'use strict';

      $('.hometexts').flexslider({
        animation: "slide",
		selector: ".slide-text .hometext",
		controlNav: false,
		directionNav: false ,
		slideshowSpeed: 4000,
		touch: true,
		direction: "vertical",
        before: function(slider){        
		 var height = $('.hometexts').find('.flex-viewport').height();
		 $('.hometext').css({ height: height + 'px' });
        }		
      });
});

/* ==============================================
Flex Slider Home Page Animated Version
=============================================== */	
$(window).load(function(){
	  'use strict';
		
      $('.hometexts-1').flexslider({
        animation: "fade",
		selector: ".slide-text-1 .hometext-1",
		controlNav: false,
		directionNav: false,
		slideshowSpeed: 5000,		
		direction: "horizontal",
        start: function(slider){
         $('body').removeClass('loading'); 
        }
      });
});

/* ==============================================
Flex Slider Home Page V5
=============================================== */	
$(window).load(function(){
	  'use strict';
		
      $('.hometexts-5').flexslider({
        animation: "fade",
		selector: ".slide-text-5 .hometext-5",
		controlNav: false,
		directionNav: true ,
		slideshowSpeed: 5000,  
		direction: "horizontal",
        start: function(slider){
         $('body').removeClass('loading'); 
        }
      });
});

/* ==============================================
Home Super Slider (images)
=============================================== */
$('#slides').superslides({
	animation: 'fade',
});

 /* ==============================================
Navigation Script
=============================================== */
jQuery(document).ready(function($) { 
 // Cache selectors
 var lastId;
 var topMenu = $(".nav"); 
 var topMenuHeight = 100; //topMenu.outerHeight()+15;
     // All list items
 var menuItems = topMenu.find("a"),
     // Anchors corresponding to menu items
     scrollItems = menuItems.map(function(){
       var item = $($(this).attr("href"));
       
       if (item.length) { return item; }
 });
  
 // Bind click handler to menu items
 // so we can get a fancy scroll animation
 menuItems.click(function(e){
  e.preventDefault();
   var href = $(this).attr("href"),
	   offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
	     
   $('html, body').stop().animate({ 
	   scrollTop: offsetTop
   }, 1000);
  //return false;
 });

$(window).scroll( function () {
		var fromTop = $(this).scrollTop()+topMenuHeight;
		var cur = scrollItems.map(function(){
		  if ($(this).offset().top < fromTop)
			return this;
		});
		cur = cur[cur.length-1];
		var id = cur && cur.length ? cur[0].id : "";
		if (lastId !== id) {
			lastId = id;
			menuItems
			  .parent().removeClass("active");
			  menuItems.filter("[href=#"+id+"]").parent().addClass("active");               
		}              
	});
});

// Menus hide after click --  mobile devices
$(document).ready(function() {  
	$('.nav li a').click(function () {
		 $('.navbar-collapse').removeClass('in');
	});
});

jQuery(window).load(function() {    
    var screenheight=jQuery(window).height()-100;
    $('#slides').css('height',screenheight+'px'); 	
    jQuery(window).scroll(function(){
       var wtop= jQuery(window).scrollTop();
       
       if(wtop > screenheight){
        jQuery('header').addClass('fix');
       }else{
        jQuery('header').removeClass('fix');
       }
    });
});	

/* ==============================================
Light-box Effect
=============================================== */
$(document).ready(function(){			  
	$("area[data-rel^='prettyPhoto']").prettyPhoto();
	$(".port-gallery:first a[data-rel^='prettyPhoto']").prettyPhoto({animation_speed:'normal',theme:'pp_default',slideshow:3000,hook: 'data-rel', autoplay_slideshow: false});
	$(".port-gallery:gt(0) a[data-rel^='prettyPhoto']").prettyPhoto({animation_speed:'fast',slideshow:10000,hook: 'data-rel', hideflash: true});
});
		  
/* ==============================================
Portfolio Effect
=============================================== */
$(document).ready(function(){

	$('.prjGrid .item').click(function() {
	var id_item = this.id;

	var id = '#i' + this.id;

	$('.next_slide').fadeIn('slow', function() {});
	$('.prev_slide').fadeIn('slow', function() {});

	var ids = '';
	var this_id = '';

	$('html,body').animate({scrollTop: pC("#work").offset().top},'slow');
	var pcC=$('.item-bg').height();
	
	$('#project .selected').animate({
		height: 0
	}, 600, function() {});
				
	$('#project .selected').removeClass('selected');
	$(id).addClass("selected");

	$(id).stop().animate({
		height: pcC
		}, 600, function() {
			$('#project .close_item_info').click(function() {
				$(id).removeClass("selected");
				$(id).stop().animate({
				height: 0
				}, 600, function() {});
			}); 
	});
	
   $('#project').stop().animate({
	  opacity: 1,
	  height: pcC
	  }, 600, function() {
		 $('.prjGrid .hover').stop().animate({ opacity: 0.5 }, 400);
		 $('.prjGrid .hover').hover(function () {
		 $(this).stop().animate({ opacity: 1 }, 200);},
			function () {
				 $(this).stop().animate({ opacity: 0.5 }, 200);}
			  );
		$('#project .close_item_info').click(function() {
		$('#project').stop().animate({
			opacity: 0,
			height: 0
			}, 600, function() {
				$('.prjGrid .hover').stop().animate({ opacity: 0 }, 400);
				$('.prjGrid .hover').hover(function () {
					$(this).stop().animate({ opacity: 1 }, 200);},
					function () {
					$(this).stop().animate({ opacity: 0 }, 200);}
					);
				});
			 });
		 });
	 });

});
			
/* ==============================================
Parallax Calling
=============================================== */
( function ( $ ) {
'use strict';
$(document).ready(function(){
$(window).bind('load', function () {
		parallaxInit();						  
	});
	function parallaxInit() {
		testMobile = isMobile.any();
		if (testMobile == null)
		{			
			$('.parallax1bg').parallax("50%", 0.5);
			$('.parallax2bg').parallax("50%", 0.5);
			$('.parallax3bg').parallax("50%", 0.5);
			$('.parallax4bg').parallax("50%", 0.5);			
		}
	}	
	parallaxInit();	 
});	
//Mobile Detect
var testMobile;
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
}( jQuery ));

/* ==============================================
Animated Items
=============================================== */	
jQuery(document).ready(function($) {
	
	'use strict';

    	$('.animated').appear(function() {
	        var elem = $(this);
	        var animation = elem.data('animation');
	        if ( !elem.hasClass('visible') ) {
	        	var animationDelay = elem.data('animation-delay');
	            if ( animationDelay ) {

	                setTimeout(function(){
	                    elem.addClass( animation + " visible" );
	                }, animationDelay);

	            } else {
	                elem.addClass( animation + " visible" );
	            }
	        }
	    });
});

/* ==============================================
Canvas Generator
=============================================== */
jQuery( document ).ready(function($) {
	canvasElementsInit();
});
function canvasElementsInit() {
		$( '.canvas-color' ).each(function() {
			var loadfunction;
			loadfunction = $(this).data('load');
			div_id = this.id;
			if( loadfunction == "drawcolor" ) {
				window[loadfunction](div_id, $(this).data('width'), $(this).data('height'), $(this).data('color'), $(this).data('hvcolor'), $(this).data('brcolor'), $(this).data('border'), $(this).data('brhvcolor'), $(this).data('icon'), $(this).data('iconsize'), $(this).data('iconcolor'), $(this).data('container'), $(this).data('animate'), '', $(this).data('link'), $(this).data('target') );
			}
		});			
			
		$( '.canvas-image' ).each(function() {
			var loadimgfunction;
			loadimgfunction = $(this).data('load');
			div_img_id = this.id;
			if( loadimgfunction == "drawimage" ) {	
				
				if( $(this).data('hvimg') != '' ) {
					var sources = {
						darthVader: $(this).data('img'),
						yoda: $(this).data('hvimg') 
					};
				} else {
					var sources = {
						darthVader: $(this).data('img'),					
					};
				}
				
				var width = $(this).data('width');
				var height = $(this).data('height');
				 
				loadImages(sources, draw, div_img_id, width, height );
				
			}
		});
		
		$( '.canvas-logo' ).each(function() {
			var loadlogofunction;
			loadlogofunction = $(this).data('load');
			div_img_id = this.id;
			if( loadlogofunction == "drawlogo" ) {
			
				if( $(this).data('hvimg') != '' ) {
					var sources = {
						darthVader: $(this).data('img'),					
						yoda: $(this).data('hvimg') 
					};
				} else {
					var sources = {
						darthVader: $(this).data('img'),					
					};
				}
				
				var width = $(this).data('width');
				var height = $(this).data('height');
				var color = $(this).data('color');
				var brcolor = $(this).data('brcolor');
				var border = $(this).data('border');
				var hlink = $(this).data('link');
				
				loadlogoImages(sources, drawlogo, div_img_id, width, height, color, brcolor, border, hlink );
									
			}
		});
}

/* ==============================================
Contact Form
=============================================== */
jQuery( document ).ready(function($) {
	$('#contactform').bootstrapValidator({
        message: '',
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {            
			contact_name: {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
			contact_email: {
                validators: {
                    notEmpty: {
                        message: ''
                    },
                    emailAddress: {
                        message: ''
                    }
                }
            },			
			contact_message: {
                validators: {
                    notEmpty: {
                        message: ''
                    }                    
                }
            }
        },
		submitHandler: function(validator, form, submitButton) {
			
			$('.vivid-contact-form').addClass('ajax-loader');
			
			var data = $('#contactform').serialize();
			
			$.ajax({
					type: "POST",
					url: "process.php",					
					data: $('#contactform').serialize(),
					success: function(msg){
						$('.vivid-contact-form').removeClass('ajax-loader');
						$('.vivid-form-message').html(msg);
						$('.vivid-form-message').show();
						submitButton.removeAttr("disabled");
						resetForm($('#contactform'));
					},
					error: function(msg){
						$('.vivid-contact-form').removeClass('ajax-loader');
						$('.vivid-form-message').html(msg);
						$('.vivid-form-message').show();
						submitButton.removeAttr("disabled");
						resetForm($('#contactform'));
					}
             });
			 
			return false;
        },
    });
	
	function resetForm($form) {
		$form.find('input:text, input:password, input, input:file, select, textarea').val('');
		$form.find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
	}
	
});
	
/* ==============================================
Fixed Menu on Scroll
=============================================== */
jQuery(document).ready(function($) {								
	$("#navigation").sticky({topSpacing:0});
});